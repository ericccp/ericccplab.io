// @ts-check
import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
    site: 'https://ericccp.gitlab.io',
    base: '/ericccp.gitlab.io',
    outDir: 'public',
    publicDir: 'static',
});
