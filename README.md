# Astro Starter Kit: Basics

```sh
npm create astro@latest -- --template basics
```

![just-the-basics](https://github.com/withastro/astro/assets/2244813/a0a5533c-a856-4198-8470-2d67b1d7c554)

## 🚀 Project Idea

This will serve as a personal portfolio for my projects, skills, and passions.
